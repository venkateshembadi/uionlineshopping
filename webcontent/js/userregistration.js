/**
 * @description User registration and validations
   * @author venkat
 */
const registerUser = () => {

    const name = document.getElementById('name').value;
    const email = document.getElementById('email').value;
    const pwd = document.getElementById('pwd').value;
    const mobileNumber = document.getElementById('mobileNumber').value;
    const pan = document.getElementById('pan').value;
    const address = document.getElementById('address').value;

    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const pattern = '[A-Za-z0-9._%+-]*(@dbs.com|@hcl.com)';
    const pwdPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if (name === '') {
        alert('Please enter your name.');
        name.focus();
        return false;
    }

/**
 * @description password validation must contain one lowercase letter,one capital letter, one Number,one special charecter.
 */

    else if (pwd === '' || !pwd.match(pwdPattern)) {
        alert('Please enter a valid pwd.');
        pwd.focus();
        return false;
    }

    else if (email === '' || !email.match(mailformat) || !email.match(pattern)) {
        alert('Please enter a valid e-mail address.');
        email.focus();
        return false;
    }


    else if (mobileNumber === '' || mobileNumber.length != 10) {
        alert('Please enter a valid Mobile');
        mno.focus();
        return false;
    }

    const obj = {
        name,
        email,
        pwd,
        mobileNumber,
        pan,
        address
    };

    let httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
    }
    let promise = new Promise(function (resolve, reject) {
        httpRequest.onreadystatechange = function () {
            if (this.readyState === 4){
            if(this.status === 201) {
                resolve('User Registered successfully');

            }
            else {
                reject(`Error, status code ${this.status}`)
            }
        }
    }
        const url = 'http://localhost:3000/users';
        httpRequest.open('post', url, true);
        httpRequest.setRequestHeader('Content-type', 'application/json');
        httpRequest.send(JSON.stringify(obj));
    });
    promise.then((response) => {
        console.log(response);
    }).catch((error) => {
        console.log(error);
    })
}