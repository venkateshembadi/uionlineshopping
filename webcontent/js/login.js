
/**
 * @description User registration and validations
   * @author venkat
 */


import {Constant} from "./localconstant.js";
const validateUser = () => {
    import { myconstant } from constant.js;
    const emailId = document.getElementById('email').value;
    const password = document.getElementById('pwd').value;
    const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    const pattern = '[A-Za-z0-9._%+-]*(@dbs.com|@hcl.com)';
    const pwdPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

    localStorage.setItem('LOCALHOST', 'http://localhost:3000');

    if (password === '' || !password.match(pwdPattern)) {
        alert('Please enter a valid pwd.');
        pwd.focus();
        return false;
    }

    else if (emailId === '' || !emailId.match(mailformat) || !emailId.match(pattern)) {
        alert('Please enter a valid e-mail address.');
        email.focus();
        return false;
    }


    let httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
    }
    var promise = new Promise(function (resolve, reject) {
        httpRequest.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let data = JSON.parse(this.response);
                console.log(data);
                let len = data.length;
                if (len > 0) {
                    //sessionStorage.setItem('email', emailId);
                   // console.log('Before redirecting url');
                   // window.location.assign('productdetails.html');
                resolve(data);

                } else {
                    reject(`Error, status code ${this.status}`)
                }
            }

        }
        // const url = 'http://localhost:3000/users?email='+emailId+'&password='+password;
        const localhost=localStorage.getItem('LOCALHOST');
        const url = `${localhost}/users?email=${emailId}&password=${password}`;
        console.log(url);
        httpRequest.open('get', url, true);
        httpRequest.send();

    });
    promise.then((response) => {
        sessionStorage.setItem('email', emailId);
        console.log('Before redirecting url');
        window.location.assign('productdetails.html');
        console.log(response);
    }).catch((error) => {
        localStorage.clear();
        console.log(error);
    })
}

