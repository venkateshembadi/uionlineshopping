/**
 * @description Display My cart product list
 */
const myCartProductsList = () => {
    let httpRequest;
    if (window.XMLHttpRequest) {
        httpRequest = new XMLHttpRequest()
    } else {
        httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
    }
    let promise = new Promise(function (resolve, reject) {
        httpRequest.onreadystatechange = function () {
            if (this.readyState === 4) {
                if (this.status != 200) {
                    reject(`Error, status code ${this.status}`);
                }
                else {
                    let tableEl = document.getElementsByTagName('table');
                    if (tableEl[0] !== undefined) {
                        tableEl[0].remove()
                    }
                    let table = document.createElement('table');

                    let tbody = document.createElement('tbody');

                    let thead = document.createElement('thead');

                    let tr = document.createElement('tr');

                    let td1 = document.createElement('td');
                    let thName1 = document.createTextNode('Id');
                    td1.append(thName1);
                    td1.style.border = '1px solid #000000';
                    td1.style.backgroundColor = '1px solid #808080';



                    let td2 = document.createElement('td');
                    let thPName = document.createTextNode('Product Name');
                    td2.append(thPName);
                    td2.style.border = '1px solid #000000';
                    td2.style.backgroundColor = '1px solid #808080';

                    let td3 = document.createElement('td');
                    let thPrice = document.createTextNode('Price');
                    td3.append(thPrice);
                    td3.style.border = '1px solid #000000';

                    let td4 = document.createElement('td');
                    let thDescription = document.createTextNode('Description');
                    td4.append(thDescription);
                    td4.style.border = '1px solid #000000';


                    let td5 = document.createElement('td');
                    let thRating = document.createTextNode('Rating');
                    td5.append(thRating);
                    td5.style.border = '1px solid #000000';

                    let td6 = document.createElement('td');
                    let thPid = document.createTextNode('PId');
                    td6.append(thPid);
                    td6.style.border = '1px solid #000000';

                    let td7 = document.createElement('td');
                    let thQuantity = document.createTextNode('Quantity');
                    td7.append(thQuantity);
                    td7.style.border = '1px solid #000000';

                    let td8 = document.createElement('td');
                    let thAction = document.createTextNode('Action');
                    td8.append(thAction);
                    td8.style.border = '1px solid #000000';


                    table.style.border = '5px solid #000000';
                    table.width = '1350px'

                    table.align = 'center';


                    tr.append(td1);
                    tr.append(td2);
                    tr.append(td3);
                    tr.append(td4);
                    tr.append(td5);
                    tr.append(td6);
                    tr.append(td7);
                    tr.append(td8);
                    thead.appendChild(tr);
                    table.appendChild(thead);
                    table.appendChild(tbody);


                    let data = JSON.parse(this.response);
                    const length = data.length;
                    if (length > 0) {
                        for (let i = 0; i < length; i++) {

                            let tableBody = document.createElement('tr');

                            let td1Body = document.createElement('td');
                            let textNode1 = document.createTextNode(data[i].id);
                            td1Body.append(textNode1);

                            let td2Body = document.createElement('td');
                            let textNode2 = document.createTextNode(data[i].productName);
                            td2Body.append(textNode2);

                            let td3Body = document.createElement('td');
                            let textNode3 = document.createTextNode(data[i].price);
                            td3Body.append(textNode3);

                            let td4Body = document.createElement('td');
                            let textNode4 = document.createTextNode(data[i].description);
                            td4Body.append(textNode4);

                            let td5Body = document.createElement('td');
                            let textNode5 = document.createTextNode(data[i].rating);
                            td5Body.append(textNode5);

                            let td6Body = document.createElement('td');
                            let textNode6 = document.createTextNode(data[i].pid);
                            td6Body.append(textNode6);

                            let td7Body = document.createElement('td');
                            let textNode7 = document.createTextNode(data[i].quantity);
                            td7Body.append(textNode7);


                            let td8Body = document.createElement('td');
                            let orderButton = document.createElement('button');
                            orderButton.style.backgroundColor = '#0000FF';
                            let orderTextNode = document.createTextNode('Order Item');
                            orderButton.appendChild(orderTextNode);

                            let td9Body = document.createElement('td');
                            let deleteButton = document.createElement('button');
                            deleteButton.style.backgroundColor = '#0000FF';
                            let deleteTextNode = document.createTextNode('Delete Cart');
                            deleteButton.appendChild(deleteTextNode);
                            /* In user cart list,user can delete cart and place order
                            order place
                            */
                            orderButton.addEventListener('click', function () {
                                let data = this.parentElement.parentElement.cells;
                                let obj1 = { pid: data[0].innerHTML, productName: data[1].innerHTML, price: data[2].innerHTML, description: data[3].innerHTML, rating: data[4].innerHTML, quantity: data[5].innerHTML, date: data[5].innerHTML, uname: data[5].innerHTML }
                                let addcartID = data[0].innerHTML;
                                localStorage.setItem('addcartID', addcartID);
                                localStorage.setItem('cartitem', JSON.stringify(obj1));
                                window.location.assign('orderitemcartlist.html');


                            });
                            /*Delete cart 
                            */
                            deleteButton.addEventListener('click', function () {
                                let data = this.parentElement.parentElement.cells;
                                let cartId = data[0].innerHTML;
                                console.log(cartId);
                                let httpRequest;

                                if (window.XMLHttpRequest) {
                                    httpRequest = new XMLHttpRequest()
                                } else {
                                    httpRequest = new ActiveXObject('Microsoft.XMLHTTP');
                                }
                                httpRequest.onreadystatechange = function () {
                                    if (this.readyState === 4 && this.status === 200) {
                                        confirm('are you sure want to delete record');
                                        myCartProductsList();
                                    }
                                }
                                const url = 'http://localhost:3000/cart/' + data[0].innerHTML;
                                httpRequest.open('delete', url, true);
                                httpRequest.send();


                            });
                            // orderButton.appendChild(orderButtonTextNode);
                            td8Body.append(orderButton);
                            td9Body.append(deleteButton)

                            td1Body.style.border = '1px solid #000000';
                            td1Body.height = '40px';

                            td2Body.style.border = '1px solid #000000';
                            td2Body.height = '40px';

                            td3Body.style.border = '1px solid #000000';
                            td3Body.height = '40px';

                            td4Body.style.border = '1px solid #000000';
                            td4Body.height = '40px';

                            td5Body.style.border = '1px solid #000000';
                            td5Body.height = '40px';

                            td6Body.style.border = '1px solid #000000';
                            td6Body.height = '40px';

                            td7Body.style.border = '1px solid #000000';
                            td7Body.height = '40px';

                            td8Body.style.border = '1px solid #000000';
                            td8Body.height = '40px';

                            td9Body.style.border = '1px solid #000000';
                            td9Body.height = '40px';


                            tableBody.append(td1Body);
                            tableBody.append(td2Body);
                            tableBody.append(td3Body);
                            tableBody.append(td4Body);
                            tableBody.append(td5Body);
                            tableBody.append(td6Body);
                            tableBody.append(td7Body);
                            tableBody.append(td8Body);
                            tableBody.append(td9Body);
                            tbody.append(tableBody);

                        }
                    }

                    else {
                        let h4data = document.createElement('h4');
                        let childNode = document.createTextNode('No Data Available in Data base');
                        h4data.appendChild(childNode);
                        tbody.appendChild(h4data);
                    }

                    let body = document.getElementsByTagName('body')[0];
                    body.appendChild(table);


                }

            }
        }


        let uname = sessionStorage.getItem('email');
        const url = `http://localhost:3000/cart?uname=${uname}`;
        console.log(url);
        //httpRequest.open('get', 'http://localhost:3000/cart', true);
        httpRequest.open('get', url, true);
        httpRequest.send();
    });
    promise.then((response) => {
        console.log(response);//create table here(create method sepearate)
    }).catch((error) => {
        console.log(error);
    })
}