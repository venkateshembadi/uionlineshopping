Title:Online Shopping 

Files:
1.login.html(user login)
2.userregistration.html(user registration)
Note:user registration validations:i)One lower case charecter ii)One capital iii)One Number iv)One special charecter v)Minimum 8length
3.productdetails.html:Display list of products
4.orderitem.html:place order 
5.addcart.html:add to cart list
6.myorderlist.html:Display list of my order list
7.mycartlist.html:Display list of my cart list

Schema and Tables:

Tables:
1.users: Users Data
2.products:list of products
3.cart:user cart data
4.orders:Order place data

Technologies Used:
HTML5,CSS3 and Javascript.
JavaScripts concepts:
i)let ii)const iii)Arrow functions iv)promises v)object literals



